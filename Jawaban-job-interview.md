# no.1
Monitoring Resource dari Komputer

**RAM**

Untuk memonitor penggunaan RAM, bisa menggunakan command free. 
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots1.jpg)
Saat mengetikan command free, akan terlihat total terpakai dan memori yang tersisa berdasarkan kilobyte.

Apabila ingin mengubah ke dalam megabyte bisa menggunakan command free--megabyte.
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots2.jpg) 

**CPU**

Untuk memonitor penggunaan CPU, bisa menggunakan command top
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots3.jpg)

**Hardisk**

Untuk memonitor penggunaan Hardisk, bisa menggunakan command df.
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots4.jpg)

# no.2

Manajemen Program 

Dapat memonitor program yang sedang berjalan untuk memonitor program yang sedang berjalan pada komputer kita dapat melihtanya menggunakan command ps atau top.
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots5.jpg)

Dapat mengehentikan program yang sedang berjalan untuk memberitahukan programnya, kita perlu menggunakan command kill dengan lalu masukan PID nya.
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots6.jpg)
hasilnya failed : operation not permitted. Karena pemberhentian program hanya bisa dilakukan oleh pemilik server sh yang saya gunakan.

Otomasi Perintah dengan Shell Script. 
Langkah pertama kita buat sebuah file scriptnya dengan menggunakan command touch, misalkan buat file cio.sh lalu setelah berhasil dibuat gunakan command vim cio.sh.Setelah itu ketikan #!/bin/bash keterangan dan $1 (parameter).
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots8.jpg)
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots7.jpg)
Untuk menjalankan shell scriptnya ketikkan ./<nama.file>.sh 
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots9.jpg)

Penjadwalan eksekusi program dengan cron untuk menjadwalkan eksekusi program kita bisa menggunakan command crontab-e.
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots10.jpg)

# no.3

Mengakses sistem operasi pada jaringan menggunakan SSH
untuk mengakses sistem operasi pada ssh, kita bisa login manual pada ssh dan mengetikan nama server juga passwordnya 
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots13.jpg)

Mampu memonitor program yang menggunakan network
untuk memonitor program menggunakan network kita bisa menggunakan command netstat -tulpn (untuk mengeluarkan beberapa program yang menggunakan network) dan netstat -tulpn|grep LISTEN (untuk mengeluarkan beberapa program yang menggunakan network pada state LISTEN) 
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots14.jpg)

Mampu mengoperasikan HTTP client
untuk melihat operasi pada http client kita bisa menggunakan command curl 
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots15.jpg)

selanjutnya untuk mendownload gambar dan sebagainya bisa menggunakan wget karna disini saya menggunakan website instagram jadi tampilan yang keluar 
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots16.jpg)

# no.4

Manajemen File dan Folder
Terdapat 3 command yaitu cd untuk masuk kepada direktori, ls untuk melihat isi direktori, dan pwd untuk melihat keberadaan direktori kita sekarang.
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots11.jpg)
CRUD file dan folder untuk editor kita bisa menggunakan (mkdir) untuk membuat folder lalu (touch) untuk melihat apa yang sudah di tambahkan (ls) untuk update atau mengganti nama file kita gunakan command (mv)untuk membuat file,(rmdir) menghapus folder, (rm) untuk menghapus file
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots12.jpg)
Untuk otomasi bisa gunakan shell script langkah - langkahnya sama seperti perintah no.2
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots18.jpg)
tampilan coba.sh
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots19.jpg)

Manajemen ownership dan akses file dan folder
untuk memenejemen ownership dan akses file dan forlder kita bisa melihat nya terlebih dahulu menggunakan command ls -l.Menggantinya menggunakan chmod (jumlah angka ganti akses owner) nama file. nilai r(read ) adalah 4 nilai w (write) adalah 2 dan x (execute) adalah 1 jadi tinggal di ganti sesuai kebutuhan
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots20.jpg)
Tampilan hasil mengganti ownership 
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots21.jpg)

Pencarian file dan folder 
Untuk mencari file dan folder bisa menggunakan find . Contoh penggunaan find:
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots24.jpg)

Kompresi data 
Untuk melakukan kompresi data, bisa menggunakan command tar dan gzip,
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots25.jpg)
Stelah diubah formatnya dibelakang file muncul .tar


# no.5
Mampu membuat program berbasis CLI menggunakan shell script
Tampilan VIM
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots7.jpg)
Tampilannya
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots9.jpg)

# no.6
Mendemonstrasikan program CLI yang dibuat kepada publik dalam bentuk Youtobe (https://youtu.be/oOSl2yEn-V0)


# no.7
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots26.jpg)

![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots27.jpg)

![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots28.jpg)

![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots29.jpg)

![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/Screenshots30.jpg)



