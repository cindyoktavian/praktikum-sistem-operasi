# no.1 

# no.2 

[dockerfile](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/Dockerfile)
[dockercompose](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/docker-compose.yml)
[hadits-d](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/hadits-d.py)
[requirements](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/requirements.txt)

Dockerfile digunakan untuk membuat Docker Image yang akan digunakan dalam kontainerisasi aplikasi "Hadits Digital" menggunakan Streamlit.

Berikut ini adalah penjelasan langkah-langkah pembuatan Docker Image melalui Dockerfile tersebut:

1. Base Image: Pertama, Dockerfile dimulai dengan pernyataan `FROM debian:bullseye-slim`. Ini menentukan bahwa Docker Image akan didasarkan pada imajinasi Debian dengan versi "bullseye-slim". Base Image menyediakan sistem operasi dasar untuk menjalankan kontainer.

2. Install Dependensi: Selanjutnya, pernyataan `RUN apt-get update && apt-get install -y git neovim netcat python3-pip` digunakan untuk mengupdate paket dan menginstal dependensi yang diperlukan dalam kontainer. Dependensi yang diinstal termasuk git, neovim, netcat, dan python3-pip.

3. Copy File Aplikasi: Pernyataan `COPY hadits-d.py /home/hadits-digital/hadits-d.py` dan `COPY requirements.txt /home/hadits-digital/requirements.txt` digunakan untuk menyalin file aplikasi "hadits-d.py" dan file requirements.txt ke dalam kontainer. File aplikasi dan file dependensi ini akan digunakan dalam langkah-langkah selanjutnya.

4. Install Dependensi Python: Pernyataan `RUN pip3 install --no-cache-dir -r /home/hadits-digital/requirements.txt` digunakan untuk menginstal dependensi Python yang diperlukan dalam aplikasi. File requirements.txt berisi daftar dependensi Python yang dibutuhkan oleh aplikasi.

5. Set Working Directory: Pernyataan `WORKDIR /home/hadits-digital` menetapkan direktori kerja di dalam kontainer sebagai "/home/hadits-digital". Ini adalah direktori di mana file-file aplikasi dan dependensi akan berada.

6. Expose Port: Pernyataan `EXPOSE 25028` digunakan untuk mengekspos port 25028 di dalam kontainer. Port ini akan digunakan untuk mengakses aplikasi "Hadits Digital" yang berjalan dalam kontainer.

7. Start Web Service: Pernyataan `CMD ["streamlit", "run", "--server.port=25028", "hadits-d.py"]` digunakan untuk menjalankan perintah saat kontainer dimulai. Pada kasus ini, perintah `streamlit run --server.port=25028 hadits-d.py` akan dijalankan untuk menjalankan aplikasi Streamlit.

    Setelah Dockerfile selesai, Anda dapat menggunakan perintah `docker build` untuk membangun Docker Image dari Dockerfile tersebut. Contoh perintahnya adalah:

```
docker build -t hadits-digital:1.0 .
```
Dengan demikian, Docker Image akan dibangun dan siap untuk digunakan dalam kontainerisasi aplikasi "Hadits Digital" menggunakan Streamlit.

# no.3

Program tersebut adalah sebuah aplikasi sederhana yang menggunakan framework Streamlit dan library Pandas di Python untuk membuat tampilan interaktif yang memungkinkan pengguna untuk mencari dan menampilkan hadits digital.  Data hadits disimpan dalam bentuk dictionary yang terdiri dari tiga kolom, yaitu 'Nomor', 'Sanad', dan 'Matan'. Setiap kolom berisi beberapa entri yang mewakili nomor hadits, sanad (periwayat), dan matan (teks hadits).Membuat input teks menggunakan st.text_input() untuk memungkinkan pengguna memasukkan nomor hadits yang ingin dicari

![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/UasPrakso.jpg)

[url web page](http:/135.181.26.148:25028)

Langkah - langkah :

1. Persiapan project
-Buat direktori untuk project dan masuk 
- Buat file **Dockerfile** untuk mendefinisikan Docker image yang akan digunakan
- Buat file **Requirements.txt** untuk mengelola dependensi Python yang di perlukan
- Buat file **hadits-d.py** yang berisi kode aplikasi web.

[dockerfile](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/Dockerfile)
[hadits-d](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/hadits-d.py)


# no.4
Docker Compose adalah alat yang digunakan untuk mendefinisikan dan menjalankan aplikasi multi-container dengan mudah. Ini memungkinkan Anda untuk mengonfigurasi dan menjalankan beberapa kontainer dalam satu lingkungan dengan pengaturan yang konsisten.

Terdapat satu layanan yang disebut "hadits-digital" yang menggunakan gambar docker "cindyoktavian/hadits-digital:1.0". Layanan ini akan dijalankan sebagai sebuah kontainer.
Berikut adalah penjelasan untuk bagian-bagian konfigurasi tersebut:
- `version: "3"`: Mendefinisikan versi format konfigurasi Docker Compose yang digunakan.
- `services`: Menyediakan definisi layanan atau kontainer yang akan dijalankan.
- `hadits-digital`: Nama layanan atau kontainer yang akan dibuat.
  - `image: cindyoktavian/hadits-digital:1.0`: Menentukan gambar Docker yang akan digunakan untuk membuat kontainer. Gambar ini akan diunduh dari Docker Hub jika belum ada di sistem.
  - `ports: - "25028:25028"`: Mengaitkan port di dalam kontainer (25028) dengan port pada host (25028). Dalam hal ini, kontainer akan menerima permintaan yang masuk pada port 25028 pada host dan mengarahkannya ke port 25028 di dalam kontainer.
  - `networks: - niat-yang-suci`: Menetapkan layanan ini ke jaringan yang diberi nama "niat-yang-suci". Jaringan ini diatur di luar file Docker Compose, dan layanan ini akan terhubung ke jaringan tersebut.
  - `volumes: - /home/praktikumc/1217050028/hadits-digital:/hadits-digital`: Mengaitkan volume host (/home/praktikumc/1217050028/hadits-digital) dengan volume dalam kontainer (/hadits-digital). 
- `networks`: Mendefinisikan jaringan yang digunakan oleh layanan atau kontainer.
  - `niat-yang-suci`: Nama jaringan yang telah dibuat di luar file Docker Compose dan akan digunakan oleh layanan "hadits-digital". Dengan menggunakan `external: true`, Docker Compose akan mencari jaringan ini di luar file konfigurasi.

Dengan menggunakan Docker Compose,  dapat menjalankan seluruh aplikasi atau layanan yang terdapat dalam konfigurasi ini dengan menjalankan perintah `docker-compose up`. Docker Compose akan membuat dan menjalankan kontainer berdasarkan konfigurasi yang diberikan, dengan pengaturan jaringan dan volume yang ditentukan.

[dockerfile](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/Dockerfile)
[dockercompose](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/docker-compose.yml)
[hadits-d](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/hadits-d.py)
[requirements](/https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/blob/main/requirements.txt)


# no.5
- Deskripsi project
Program tersebut adalah sebuah aplikasi sederhana yang menggunakan framework Streamlit dan library Pandas di Python untuk membuat tampilan interaktif yang memungkinkan pengguna untuk mencari dan menampilkan hadits digital. Berikut ini adalah penjelasan lebih detail mengenai setiap bagian dari program tersebut:
1. Impor library: Program dimulai dengan mengimpor library yang diperlukan yaitu `pandas` dan `streamlit`.
2. Data hadits digital: Data hadits disimpan dalam bentuk dictionary yang terdiri dari tiga kolom, yaitu 'Nomor', 'Sanad', dan 'Matan'. Setiap kolom berisi beberapa entri yang mewakili nomor hadits, sanad (periwayat), dan matan (teks hadits).
3. Menampilkan judul: Menggunakan fungsi `st.title()` dari Streamlit untuk menampilkan judul "Hadits Digital" pada tampilan aplikasi.
4. Membuat input pencarian: Membuat input teks menggunakan `st.text_input()` untuk memungkinkan pengguna memasukkan nomor hadits yang ingin dicari.
5. Filtering hasil: Program melakukan proses filtering pada data hadits berdasarkan input pencarian yang diberikan. Dalam loop `for`, setiap entri dalam kolom 'Nomor' dicek apakah cocok dengan input pencarian. Jika cocok, entri tersebut ditambahkan ke dalam list `filtered_results`.
6. Menampilkan hasil filtering: Menggunakan fungsi `st.table()` untuk menampilkan hasil filtering dalam bentuk tabel. List `filtered_results` dikonversi menjadi DataFrame menggunakan `pd.DataFrame()` agar dapat ditampilkan dengan format tabel yang rapi.

7. Menampilkan aplikasi Streamlit: Setelah proses filtering selesai, aplikasi akan menampilkan tampilan Streamlit menggunakan fungsi-fungsi Streamlit berikut:

   - `st.title()`: Menampilkan judul "Hadits Digital" pada tampilan aplikasi.
   - `st.text_input()`: Membuat input teks "Search nomor..." yang memungkinkan pengguna memasukkan nomor hadits yang ingin dicari.
   - `st.table()`: Menampilkan hasil filtering dalam bentuk tabel. DataFrame yang dihasilkan dari list `filtered_results` ditampilkan menggunakan fungsi ini.

   Ketika aplikasi dijalankan, pengguna dapat memasukkan nomor hadits yang ingin dicari dalam input teks. Setiap kali pengguna memasukkan atau mengubah nilai dalam input teks, proses filtering akan berjalan dan hasil filtering akan ditampilkan dalam bentuk tabel secara otomatis.

8. Hasil filtering: Hasil filtering yang ditampilkan dalam tabel terdiri dari tiga kolom: 'Nomor', 'Sanad', dan 'Matan'. Kolom 'Nomor' menampilkan nomor hadits yang cocok dengan pencarian, kolom 'Sanad' menampilkan sanad (periwayat) hadits, dan kolom 'Matan' menampilkan teks hadits.

    Dengan menggunakan program ini, pengguna dapat memasukkan nomor hadits yang ingin dicari, dan aplikasi akan menampilkan hasil filtering sesuai dengan nomor hadits yang cocok dengan input yang diberikan. Hasilnya ditampilkan dalam bentuk tabel pada tampilan aplikasi.
    Dengan menggunakan aplikasi ini, pengguna dapat mencari hadits berdasarkan nomor dengan mudah dan melihat sanad dan teks hadits yang sesuai dengan nomor yang dicari. Aplikasi ini dapat berguna bagi mereka yang ingin mencari hadits secara cepat dan efisien.

- Penjelasan bagaimana sistem operasi digunakan dalam proses containerization

    Sistem operasi memainkan peran penting dalam proses containerisasi. Containerisasi adalah metode pengemasan dan menjalankan aplikasi serta dependensinya dalam lingkungan yang terisolasi yang disebut container. Sistem operasi menyediakan lingkungan dasar untuk menjalankan kontainer dan memungkinkan aplikasi yang diisolasi berjalan di atasnya.

    Berikut ini adalah beberapa aspek penting dalam penggunaan sistem operasi dalam proses containerisasi:

1. Sistem operasi host: Sistem operasi host merujuk pada sistem operasi yang diinstal pada mesin fisik atau virtual di mana kontainer akan berjalan. Sistem operasi host harus memiliki dukungan untuk menjalankan teknologi kontainer seperti Docker atau Kubernetes. Umumnya, sistem operasi yang mendukung kontainer termasuk Linux (seperti Ubuntu, CentOS, atau Fedora) dan Windows 10 Pro/Enterprise atau Windows Server (untuk kontainer Windows). Sistem operasi host harus terpasang dengan runtime kontainer seperti Docker Engine atau container runtime Kubernetes seperti containerd.

2. Sistem operasi kontainer: Setiap kontainer memiliki sistem operasi yang terisolasi, yang disebut sistem operasi kontainer. Sistem operasi kontainer terdiri dari kernel Linux yang berfungsi sebagai kernel virtualisasi dan sistem file yang digunakan oleh aplikasi di dalam kontainer. Dalam containerisasi, sistem operasi kontainer tidak perlu menjadi sistem operasi yang sama dengan sistem operasi host. Misalnya, Anda dapat menjalankan kontainer dengan sistem operasi Ubuntu pada sistem operasi host Windows.

3. Imej basis (base image): Imej basis adalah imajinasi awal yang digunakan untuk membangun kontainer. Imej basis adalah sistem operasi yang mendasari sistem operasi kontainer dan menyediakan lingkungan dasar untuk menjalankan aplikasi di dalam kontainer. Imej basis umumnya berisi sistem operasi dan perangkat lunak yang diperlukan untuk menjalankan aplikasi yang diinginkan. Contoh umum dari imajinasi basis adalah imajinasi Ubuntu atau Alpine Linux. Imej basis digunakan sebagai dasar untuk membangun kontainer kustom dengan menambahkan dependensi dan kode aplikasi yang diperlukan.

4. Kontainerisasi lintas-platform: Kontainerisasi tidak terbatas pada satu sistem operasi saja. Dengan adanya teknologi seperti Docker, kontainer dapat berjalan pada berbagai sistem operasi, termasuk Linux, Windows, dan macOS. Ini memungkinkan pengembang dan tim operasi untuk mengemas dan mendistribusikan aplikasi dalam kontainer yang dapat dijalankan di berbagai sistem operasi.

    Dalam konteks containerisasi, sistem operasi berperan dalam menyediakan isolasi, keamanan, dan lingkungan yang terkapsulasi untuk menjalankan aplikasi. Sistem operasi host menyediakan infrastruktur untuk menjalankan kontainer, sementara sistem operasi kontainer menyediakan lingkungan untuk aplikasi di dalam kontainer. Penggunaan sistem operasi yang tepat pada kedua tingkatan ini memainkan peran penting dalam keberhasilan dan kinerja kontainerisasi.

- Penjelasan bagaimana containerization dapat membantu mempermudah pengembangan aplikasi

    Containerisasi dapat membantu mempermudah pengembangan aplikasi dalam beberapa cara:

1. Konsistensi lingkungan: Dalam pengembangan aplikasi, sering kali terjadi masalah yang disebabkan oleh perbedaan lingkungan antara mesin pengembangan, pengujian, dan produksi. Dengan menggunakan container, lingkungan pengembangan dapat dikemas dalam sebuah container yang konsisten. Ini berarti setiap anggota tim pengembangan dapat menggunakan lingkungan yang sama, dengan konfigurasi dan dependensi yang identik, sehingga mengurangi masalah yang mungkin timbul akibat perbedaan lingkungan.

2. Portabilitas: Kontainer adalah unit portabel yang dapat dibangun di satu lingkungan dan dijalankan di lingkungan lain tanpa perlu memperhatikan perbedaan konfigurasi infrastruktur. Ini memudahkan pengembang dalam mengembangkan aplikasi di lingkungan lokal dan kemudian memindahkannya ke lingkungan produksi atau pengujian dengan cepat dan mudah. Pengembang tidak perlu menghabiskan waktu dan upaya dalam mengonfigurasi dan menyelaraskan lingkungan di setiap tahap pengembangan.

3. Reproduksi yang konsisten: Container menyediakan cara yang terstandarisasi untuk mengemas dan mendistribusikan aplikasi beserta dependensinya. Dengan menggunakan file konfigurasi yang dikenal sebagai Dockerfile atau manifestasi serupa, pengembang dapat mendefinisikan lingkungan aplikasi secara terperinci dan memastikan bahwa setiap anggota tim pengembangan menggunakan lingkungan yang sama. Ini mempermudah reproduksi lingkungan secara konsisten dan meminimalkan masalah yang muncul akibat perbedaan konfigurasi.

4. Isolasi dan keamanan: Container memberikan isolasi yang lebih baik antara aplikasi dan lingkungan host. Setiap kontainer berjalan dalam lingkungan terisolasi, membatasi dampak yang mungkin terjadi pada sistem operasi host. Ini memungkinkan pengembang untuk menguji aplikasi dengan aman tanpa khawatir tentang kerusakan sistem operasi atau dependensi lainnya. Selain itu, dengan menggunakan mekanisme pengendalian akses yang tepat, container juga dapat membantu meningkatkan keamanan aplikasi dengan membatasi akses ke sumber daya sistem yang tidak diperlukan.

5. Skalabilitas: Containerisasi memungkinkan aplikasi untuk diskalakan dengan mudah. Dalam lingkungan kontainer, pengembang dapat dengan cepat menduplikasi dan mengelola banyak instance aplikasi dengan mudah menggunakan orkestrasi seperti Kubernetes. Hal ini memudahkan pengembang untuk menguji skenario dengan skala yang berbeda dan memastikan aplikasi berperilaku sebagaimana diharapkan dalam kondisi skala yang lebih besar.

    Secara keseluruhan, containerisasi membantu menyederhanakan pengembangan aplikasi dengan memberikan konsistensi lingkungan, portabilitas, reproduksi yang konsisten, isolasi, keamanan, dan skalabilitas. Ini memungkinkan pengembang untuk fokus pada pengembangan aplikasi itu sendiri tanpa harus menghabiskan banyak waktu dan upaya dalam mengelola lingkungan infrastruktur yang kompleks.

- Penjelasan apa itu DevOps, bagaimana DevOps membantu pengembangan aplikasi

    DevOps adalah suatu pendekatan kolaboratif yang menggabungkan praktik pengembangan (Development) dan operasional (Operations) untuk mempercepat pengembangan dan pengiriman aplikasi dengan peningkatan kualitas dan stabilitas.

    DevOps melibatkan kerjasama antara tim pengembangan (developers) dan tim operasional (operations) dalam semua tahapan siklus hidup aplikasi, mulai dari perencanaan, pengembangan, pengujian, hingga pengiriman dan pemeliharaan. Pendekatan ini berfokus pada otomatisasi, pengintegrasian berkelanjutan (continuous integration), pengiriman berkelanjutan (continuous delivery/deployment), serta pemantauan dan umpan balik yang berkelanjutan.

    Berikut ini adalah beberapa cara di mana DevOps membantu pengembangan aplikasi:

1. Kolaborasi Tim: DevOps menghilangkan pemisahan antara tim pengembangan dan operasional dengan mendorong kerjasama dan komunikasi yang lebih erat antara keduanya. Tim pengembangan dan operasional bekerja bersama untuk mengidentifikasi kebutuhan, menentukan prioritas, merencanakan pengembangan, serta memecahkan masalah. Hal ini membantu mengurangi gesekan dan meningkatkan efisiensi dalam pengembangan aplikasi.

2. Otomatisasi: DevOps menerapkan otomatisasi dalam seluruh siklus hidup aplikasi. Hal ini termasuk otomatisasi proses pengujian, pengemasan, pengiriman, dan penerapan aplikasi. Dengan otomatisasi yang tepat, pengembang dapat mengurangi kesalahan manusia, meningkatkan kecepatan pengiriman, dan meningkatkan konsistensi dalam setiap langkah pengembangan aplikasi.

3. Continuous Integration dan Continuous Delivery: DevOps mempromosikan praktik continuous integration (CI) dan continuous delivery (CD). Continuous integration melibatkan penggabungan dan pengujian perubahan kode secara teratur untuk mendeteksi kesalahan sejak dini. Continuous delivery memastikan bahwa perubahan yang lulus pengujian dapat dikirimkan ke produksi secara otomatis dan dengan cepat. Dengan CI/CD, pengembang dapat mengintegrasikan perubahan dengan cepat, mengurangi konflik kode, dan meningkatkan kualitas serta kecepatan pengiriman aplikasi.

4. Pemantauan dan Umpan Balik: DevOps memperkenalkan pemantauan berkelanjutan pada aplikasi yang sedang berjalan di produksi. Tim operasional dapat melacak performa dan kinerja aplikasi secara real-time, mengidentifikasi masalah, dan merespons dengan cepat. Umpan balik yang diperoleh dari pemantauan ini membantu pengembang dalam mengidentifikasi masalah dan memperbaiki perubahan yang diperlukan untuk meningkatkan kualitas dan keandalan aplikasi.

5. Skalabilitas dan Resiliensi: DevOps memperhatikan skalabilitas dan resiliensi aplikasi. Tim pengembangan bekerja sama dengan tim operasional untuk merencanakan dan mengimplementasikan arsitektur yang memungkinkan aplikasi untuk berkembang dengan lancar, menangani lonjakan lalu lintas, serta memulihkan diri dari kegagalan dengan cepat. Hal ini memastikan aplikasi tetap berjalan dengan baik saat ada pertumbuhan pengguna atau ketika terjadi gangguan.

    Dengan menerapkan pendekatan DevOps, pengembangan aplikasi dapat menjadi lebih efisien, cepat, dan terstruktur. DevOps membantu dalam mengatasi hambatan dan silo antara tim pengembangan dan operasional, mengurangi waktu pengembangan, meningkatkan kualitas aplikasi, dan meningkatkan kepuasan pengguna.

- Berikan contoh kasus penerapan DevOps di perusahaan / industri dunia nyata (contoh bagaimana implementasi DevOps di Gojek, dsb.)

    Shopee, sebagai salah satu perusahaan e-commerce terkemuka di Asia Tenggara, telah menerapkan prinsip-prinsip DevOps untuk meningkatkan efisiensi, kecepatan, dan kualitas pengembangan dan pengiriman aplikasi mereka. Berikut adalah beberapa kasus penerapan DevOps di Shopee:

1. Continuous Integration dan Continuous Deployment: Shopee menggunakan praktik continuous integration dan continuous deployment untuk mengintegrasikan dan mengirimkan perubahan kode ke lingkungan produksi secara terus-menerus. Mereka memiliki proses otomatisasi yang kuat untuk pengujian perubahan kode, memastikan kualitas perangkat lunak dan memungkinkan penyebaran perubahan dengan cepat ke pengguna.

2. Automasi Infrastruktur dan Penyebaran: Shopee telah mengadopsi alat otomasi infrastruktur seperti Kubernetes untuk mengelola dan menyebar aplikasi mereka secara efisien. Mereka menggunakan konsep infrastruktur sebagai kode (Infrastructure as Code) untuk mengelola sumber daya infrastruktur secara otomatis dan konsisten. Hal ini memungkinkan mereka untuk dengan cepat menyebarkan dan mengelola lingkungan pengujian dan produksi.

3. Pemantauan dan Pengelolaan Kinerja Aplikasi: Shopee menerapkan pemantauan berkelanjutan dan pengelolaan kinerja aplikasi dalam operasional mereka. Mereka menggunakan alat pemantauan real-time dan pemantauan jejak performa untuk memantau kinerja aplikasi secara aktif. Tim DevOps Shopee dapat dengan cepat mendeteksi masalah, mengidentifikasi penyebabnya, dan merespons dengan cepat untuk memperbaiki kinerja aplikasi.

4. Kolaborasi Tim: Shopee menerapkan kerja tim yang erat antara tim pengembangan, tim operasional, dan tim produk. Mereka mengadopsi prinsip DevOps dalam budaya kerja mereka, mempromosikan kolaborasi, komunikasi yang lancar, dan berbagi pengetahuan antar tim. Ini memungkinkan tim untuk saling bekerja sama dalam mengatasi masalah, meningkatkan efisiensi pengembangan, dan memastikan pengiriman aplikasi yang sukses.

5. Proses Pengujian dan Pelacakan Masalah: Shopee menggunakan alat pengujian otomatis dan sistem pelacakan masalah untuk meningkatkan keandalan aplikasi mereka. Mereka melakukan pengujian berkelanjutan untuk mengidentifikasi dan memperbaiki bug dan masalah yang mungkin muncul dalam pengembangan. Dengan adanya sistem pelacakan masalah yang efisien, mereka dapat memprioritaskan dan melacak status perbaikan dengan lebih baik.

    Dalam rangka menerapkan DevOps, Shopee telah berinvestasi dalam alat-alat otomatisasi, infrastruktur yang dapat diskalakan, serta budaya kerja kolaboratif. Hal ini memungkinkan mereka untuk menghadapi tantangan dalam pengembangan dan pengiriman aplikasi dengan lebih baik, meningkatkan efisiensi dan kualitas, serta memberikan pengalaman yang baik kepada pengguna mereka.

# no.6
https://youtu.be/DHRIqgOucsE

# no.7
![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/HUB2.jpg)

![image](https://gitlab.com/cindyoktavian/praktikum-sistem-operasi/-/raw/main/Image/HUB1.jpg)
(/https://hub.docker.com/repository/docker/cindyoktavian/hadits-digital/general)
