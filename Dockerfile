FROM debian:bullseye-slim

#install
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

#Copy web service files
COPY hadits-d.py /home/hadits-digital/hadits-d.py
COPY requirements.txt /home/hadits-digital/requirements.txt

#install python
RUN pip3 install --no-cache-dir -r /home/hadits-digital/requirements.txt

#set working directory
WORKDIR /home/hadits-digital

#expose port
EXPOSE 25028

#start web service
CMD ["streamlit", "run", "--server.port=25028", "hadits-d.py"]