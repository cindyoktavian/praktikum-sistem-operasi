import pandas as pd
import streamlit as st

# Data hadits digital
data = {
    'Nomor': [1,2,3,4,5],
    'Sanad': ['Abu Hurairah', 'Umar bin Khattab', ' Anas bin Malik', ' Anas bin Malik', 'Aisyah', ],
    'Matan': ['Rasullah SAW bersabda Tinggalkanlah apa yang meragukanmu untuk apa yang tidak meragukanmu', 'Rasulullah SAW bersabda,Tidak ada iman bagi orang yang tidak dapat dipercaya, dan tidak ada agama bagi orang yang tidak menjaga janji', 
              'Rasulullah SAW bersabda,Berpegang teguhlah kepada sunnahku dan sunnah para khulafa ar-raasyidin yang mendapat petunjuk. Gigitlah dengan gigi gerahammu dan berhati-hatilah terhadap perkara baru yang dibuat dalam agama', 
              'Jika seseorang meminta pertolongan kepada Allah, maka berikanlah pertolongan padanya','Tidaklah seorang dari kalian beriman sehingga ia mencintai saudaranya sebagaimana ia mencintai dirinya sendiri' ]
}

# Menampilkan judul
st.title('Hadits Digital')

# Membuat input untuk pencarian nomor
search_input = st.text_input('Search nomor...')

# Membuat list untuk menyimpan hasil filtering
filtered_results = []

# Melakukan filtering berdasarkan input pencarian
for idx, hadits in enumerate(data['Nomor']):
    if str(hadits).lower().startswith(search_input.lower()):
        filtered_results.append({
            'Nomor': data['Nomor'][idx],
            'Sanad': data['Sanad'][idx],
            'Matan': data['Matan'][idx]
        })

# Menampilkan hasil filtering
st.table(pd.DataFrame(filtered_results))
